﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitSandbox
{
    public class Calculator
    {
        public static void Main1()
        {
            Console.WriteLine("Main calc...");
        }

        public long Add(int a, int b)
        {
            return a + b;
        }

        public long Substract(int a, int b)
        {
            return a - b;
        }

        public long Multiply(int a, int b)
        {
            return a * b;
        }

        public decimal Divide(int a, int b)
        {
            return a / b;
        }
    }
}
