﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GitSandbox
{
    public class StringUtils
    {
        public static void Main2()
        {
            Console.WriteLine("Main StringUtils");
        }

        public static string AddPrefix(string text, string prefix)
        {
            return prefix + text;
        }

        public static string AddSuffix(string text, string suffix)
        {
            return text + suffix;
        }

        public static string RemoveSpaces(string text)
        {
            return text.Replace(" ", "");
        }

        public static string AddSpaces(string text)
        {
            return $" {text} ";
        }

                public static string AddSpaces(string text)
        {
            return $" {text} ";
        }
    }
}
